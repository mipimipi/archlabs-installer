# ArchLabs Installer

#### Features

- LUKS/LVM setup
- Package & Session selection
- Full device auto partition

#### Requirements

`coreutils`, `parted`, `rsync`, `dialog`, `vim`, `chpasswd`, `curl`, `arch-chroot`


#### Manual Installation

```
sh -c "$(curl -fsSL https://bitbucket.org/archlabslinux/installer/raw/master/install.sh)"
```

---
A packaged version can also be found in our repos:
[stable](https://bitbucket.org/archlabslinux/archlabs_repo/src/master/x86_64/)
[unstable](https://bitbucket.org/archlabslinux/archlabs_unstable/src/master/x86_64/)

