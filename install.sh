#!/bin/bash

hash git >/dev/null 2>&1 || { printf "This requires git installed\n"; exit 1; }

git clone --depth=1 https://bitbucket.org/archlabslinux/installer

if [[ $(whoami) != 'root' ]]; then
    asroot=sudo
    printf "\nRoot access is needed to continue\n\n"
else
    asroot=""
fi

$asroot mkdir -pv /usr/share/archlabs/installer/{lang,docs}
$asroot cp -fv installer/src/archlabs-installer /usr/bin/
$asroot cp -fv installer/src/packages.txt /usr/share/archlabs/installer/
$asroot cp -fv installer/lang/*.trans /usr/share/archlabs/installer/lang/
$asroot cp -fv installer/{LICENSE,README.md} /usr/share/archlabs/installer/docs/

printf "\nInstall complete\n"
