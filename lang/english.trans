# english translation file
# written by natemaia10@gmail.com - 2018

_WelTitle="Welcome to the"
_WelBody="\nThis will help you get $DIST setup on your system.\nHaving GNU/Linux experience is an asset, however we try our best to keep things simple.\n\nIf you are unsure about an option, a default will be listed or\nthe first selected option will be the default (excluding language and timezone).\n\n\nMenu Navigation:\n\n - Select items with the arrow keys or the option number.\n - Use [Space] to toggle options and [Enter] to confirm.\n - Switch between buttons using [Tab] or the arrow keys.\n - Use [Page Up] and [Page Down] to jump whole pages\n - Press the highlighted key of an option to select it.\n"

_PrepBody="\nThis is the menu where you can prepare your system for the install.\n\nTo begin the install you must first have:\n\n  - A root (/) partition mounted (UEFI systems also require a seperate boot partition).\n  - A new user created and the passwords set.\n  - The system configuration finished.\n\nOnce the above requirements are met and you have gone through any optional setup steps you like the install can be started."
_PrepShow="Show lsblk output (optional)"
_PrepPart="Edit partitions (optional)"
_PrepLUKS="LUKS encryption (optional)"
_PrepLVM="Logical volume management (optional)"
_PrepMnt="Mount and format partitions"
_PrepUser="Create user and set passwords"
_PrepConf="Configure system settings"
_PrepWM="Select window manager or desktop (optional)"
_PrepPkg="Select additional packages (optional)"
_PrepChk="Check configuration choices (optional)"
_Install="Start the installation"

_EditBody="\nBefore exiting you can select configuration files to review/change.\n\nIf you need to make other changes with the drives still mounted, use Ctrl-z to pause the installer, when finished type 'fg' and [Enter] or Ctrl-z again to resume the installer."

_TimeZBody="\nThe time zone is used to set the system clock.\n\nSelect your country or continent from the list below"
_TimeSubZBody="\nSelect the nearest city to you or one with the same time zone.\n\nTIP: Pressing the first letter of the city name repeatedly will navigate between entries beggining with that letter."

_MirrorSetup="\nSort the mirrorlist automatically?\n\nTakes longer but gets fastest mirrors.\n"
_MirrorCmd="\nThe command below will be used to sort the mirrorlist, edit if needed.\n"

_WMChoiceBody="\nUse [Space] to toggle available sessions.\n\nFor all sessions a basic package set will be installed for basic compatibilty across sessions. In addition to this extra packages specific to each sessions will also be installed to provide basic functionality most people expect from an environment."

_PackageMenu="\nSelect a category to choose packages from, once finished select the last entery or press [Esc] to return to the main menu."
_PackageBody="\nUse [Space] to toggle packages(s) and press [Enter] to accept the selection.\n\nNOTE: Some packages may already be installed by your desktop environment (if any). Extra packages may also be installed for the selected packages eg. Selecting qutebrowser will also install qt5ct (the Qt5 theme tool) and qt5-styleplugins (for Gtk themes in Qt applications)."

_WMLoginBody="\nSelect which of your session choices to use for the initial login.\n\nYou can be change this later by editing your ~/.xinitrc"

_XMapBody="\nPick your system keymap from the list below\n\nThis is the keymap used once a graphical environment is running (usually Xorg).\n\nSystem default: us"
_LocaleBody="\nLocale determines the system language and currency formats.\n\nThe format for locale names is languagecode_COUNTRYCODE\n\neg. en_US is: english United States\n    en_GB is: english Great Britain"

_CMapBody="\nSelect console keymap, the console is the tty shell you reach before starting a graphical environment (usually Xorg).\n\nIts keymap is seperate from the one used by the graphical environments, though many do use the same such as 'us' English.\n\nSystem default: us"

_HostNameBody="\nEnter a hostname for the new system.\n\nA hostname is used to identify systems on the network.\n\nIt's restricted to alphanumeric characters (a-z, A-Z, 0-9).\nIt can contain hyphens (-) BUT NOT at the beggining or end."

_RootBody="--- Enter root password (empty uses the password entered above) ---"
_UserBody="\nEnter a name and password for the new user account.\n\nThe name must not use capital letters, contain any periods (.), end with a hyphen (-), or include any colons (:)\n\nNOTE: Use the [Up] and [Down] arrows to switch between input fields, [Tab] to toggle between input fields and the buttons, and [Enter] to accept."

_MntBody="\nUse [Space] to toggle mount options from below, press [Enter] when done to confirm selection.\n\nNot selecting any and confirming will run an automatic mount."
_WarnMount="\nIMPORTANT: Please choose carefully during mounting and formatting.\n\nPartitions can be mounted without formatting by selecting skip during mounting, useful for extra or already formatted partitions.\n\nThe exception to this is the root (/) partition, it needs to be formatted before install to ensure system stability.\n"

_DevSelBody="\nSelect a device to use from the list below.\n\nDevices (/dev) are the available drives on the system. /sda, /sdb, /sdc ..."

_ExtPartBody="\nYou can now select additional partitions you want mounted, once choosen you will be asked to enter a mountpoint.\n\nSelect 'done' to finish the mounting step and return to the main menu."
_ExtPartBody1="\nWhere do you want the partition mounted?\n\nEnsure the name begins with a slash (/).\nExamples include: /usr, /home, /var, etc."

_PartBody="\nFull device auto partitioning is available for beginners otherwise cfdisk is recommended.\n\n  - All systems will require a root partition (8G or greater).\n  - UEFI and BIOS using LUKS without LVM will require a boot partition (100-512M)."

_PartBody1="\nWARNING: ALL data on"
_PartBody2="will be destroyed and the following partitions will be created\n\n- A vfat/fat32 boot partition with boot flags enabled (512M)\n- An ext4 partition using all remaining space"
_PartBody3="\n\nDo you want to continue?\n"
_PartWipeBody="will be destroyed using 'wipe -Ifre'.\n\nThis is ONLY intended for use on devices before sale or disposal to reliably destroy the data beyond recovery. This is NOT for devices you intend to continue using.\nThe wiping process can take a long time depending on the size and speed of the drive.\n\nDo you still want to continue?\n"

_SelRootBody="\nSelect the root (/) partition, this is where $DIST will be installed."
_SelUefiBody="\nSelect the EFI boot partition (/boot), required for UEFI boot.\n\nIt's usually the first partition on the device, 100-512M, and will be formatted as vfat/fat32 if not already."
_SelBiosBody="\nDo you want to use a separate boot partition? (optional)\n\nIt's usually the first partition on the device, 100-512M, and will be formatted as ext3/4 if not already."
_SelBiosLuksBody="\nSelect the boot partition (/boot), required for LUKS.\n\nIt's usually the first partition on the device, 100-512M, and will be formatted as ext3/4 if not already."
_FormBootBody="is already formatted correctly.\n\nFor a clean install, previously existing partitions should be reformatted, however this removes ALL data (bootloaders) on the partition so choose carefully.\n\nDo you want to reformat the partition?\n"

_SelSwpErr="\nSwap Setup Error: Must be 1(M|G) or greater, and can only contain whole numbers\n\nSize Entered:"
_SelSwpSize="\nEnter the size of the swapfile in megabytes (M) or gigabytes (G).\n\neg. 100M will create a 100 megabyte swapfile, while 10G will create a 10 gigabyte swapfile.\n\nFor ease of use and as an example it is filled in to match the size of your system memory (RAM).\n\nMust be greater than 1, contain only whole numbers, and end with either M or G."

# LUKS / DM-Crypt / Encryption
_LuksMenuBody="\nDevices and volumes encrypted using dm_crypt cannot be accessed or seen without first being unlocked."
_LuksMenuBody2="\n\nA seperate boot partition without encryption or logical volume management (LVM) is required (unless using BIOS Grub)."
_LuksMenuBody3="\n\nAutomatic uses default encryption settings, and is recommended for beginners, otherwise cypher and key size parameters may be entered manually."
_LuksOpenBody="\nEnter a name and password for the encrypted device.\n\nIt is not necessary to prefix the name with /dev/mapper/,an example has been provided."
_LuksEncrypt="Basic LUKS Encryption"
_LuksEncryptAdv="Advanced LUKS Encryption"
_LuksOpen="Open Encrypted Partition"
_LuksEncryptBody="\nSelect the partition you want to encrypt."
_LuksEncryptSucc="\nDone! encrypted partition opened and ready for mounting.\n"
_LuksPartErrBody="\nA minimum of two partitions are required for encryption:\n\n 1. root (/) - standard or LVM.\n 2. boot (/boot) - standard (unless using LVM on BIOS systems).\n"
_LuksCreateWaitBody="\nCreating encrypted partition:"
_LuksOpenWaitBody="\nOpening encrypted partition:"
_LuksWaitBody2="\n\nDevice or volume used:"
_LuksCipherKey="Once the specified flags have been amended, they will automatically be used with the 'cryptsetup -q luksFormat /dev/...' command.\n\nNOTE: Do not specify any additional flags such as -v (--verbose) or -y (--verify-passphrase)."

_LvmMenu="\nLogical volume management (LVM) allows 'virtual' hard drives (volume groups) and partitions (logical volumes) to be created from existing device partitions.\n\nA volume group must be created first, then one or more logical volumes within it.\n\nLVM can also be used with an encrypted partition to create multiple logical volumes (e.g. root and home) within it."
_LvmNew="Create VG and LV(s)"
_LvmDel="Delete existing VG(s)"
_LvmDelAll="Delete all VGs, LVs, and PVs"
_LvmDetBody="\nExisting logical volume management (LVM) detected.\n\nActivating...\n"
_LvmNameVgBody="\nEnter the name of the volume group (VG) to create.\n\nThe VG is the new virtual device that will be created from the partition(s) selected."
_LvmPvSelBody="\nSelect the partition(s) to use for the physical volume (PV)."
_LvmPvConfBody1="\nConfirm creation of volume group:"
_LvmPvConfBody2="with the following partition(s):"
_LvmPvActBody1="\nCreating and activating volume group:"
_LvmLvNumBody1="\nSelect the number of logical volumes (LVs) to create in:"
_LvmLvNumBody2="\nThe last (or only) logical volume will automatically use all remaining space in the volume group."
_LvmLvNameBody1="\nEnter the name of the logical volume (LV) to create.\n\nThis is like setting a name or label for a partition.\n"
_LvmLvNameBody2="\nNOTE: This LV will use up all remaining space in the volume group"
_LvmLvSizeBody1="remaining"
_LvmLvSizeBody2="\nEnter the size of the logical volume (LV) in megabytes (M) or gigabytes (G). For example, 100M will create a 100 megabyte LV. 10G will create a 10 gigabyte LV.\n"
_LvmCompBody="\nDone! all logical volumes have been created for the volume group.\n\nDo you want to view the device tree for the new LVM scheme?\n"
_LvmDelQ="\nConfirm deletion of volume group(s) and logical volume(s).\n\nDeleting a volume group, will delete all logical volumes within as well.\n"
_LvmSelVGBody="\nSelect volume group to delete.\n\nAll logical volumes within will also be deleted."

_LvmVGErr="\nNo volume groups found."
_LvmNameVgErr="\nInvalid name entered.\n\nThe volume group name may be alpha-numeric, but may not contain spaces, start with a '/', or already be in use.\n"
_LvmPartErrBody="\nThere are no viable partitions available to use for LVM, a minimum of one is required.\n\nIf LVM is already in use, deactivating it will allow the partition(s) to be used again.\n"
_LvmLvNameErrBody="\nInvalid name entered.\n\nThe logical volume (LV) name may be alpha-numeric, but may not contain spaces or be preceded with a '/'\n"
_LvmLvSizeErrBody="\nInvalid value Entered.\n\nMust be a numeric value with 'M' (megabytes) or 'G' (gigabytes) at the end.\n\neg. 400M, 10G, 250G, etc...\n\nThe value may also not be equal to or greater than the remaining size of the volume group.\n"

_ErrTitle="Installation Error"
_ExtErrBody="\nCannot mount partition due to a problem with the mountpoint.\n\nEnsure it begins with a slash (/) followed by atleast one character.\n"
_PartErrBody="\nYou need create the partiton(s) first.\n\n\nBIOS systems require at least one partition (ROOT).\n\nUEFI systems require at least two (ROOT and EFI).\n"

# vim:tw=9999:syntax=off:nospell
