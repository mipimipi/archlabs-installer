# Generic
_All="Tutte"
_Done="Fatto"
_Skip="Salta/Nessuno"
_Back="Indietro"
_ErrTitle="Errore"
_NoFileErr="\nIl file non esiste.\n"
_PlsWait="\nAttendere prego...\n"
_PassErr="\nLe password digitate non corrispondono.\n"
_Pass2="Immettere nuovamente la password per"
_TryAgain="Provare nuovamente.\n"
_Exit="Procedura terminata.\n"
_Name="Nome:"

# Welcome
_WelTitle="Benvenuto in"
_WelBody="\nQuesto installer scaricherà i pacchetti più recenti dai repositories $DIST. I passaggi comprenderrano la minima configurazione necessaria.\n\nOPZIONI MENU: Selezionare premendo il numero corrispondente o usando i tasti su/giù prima di premere [invio] per confermare. Scorrere fra i bottoni premendo [Tab] o le frecce sinistra/destraconfermando con [Invio]. È possibile navigare liste più lunghe usando i tasti [pg up] e [pg down], e/o la prima lettera corrispondente all'opzione selezionata.\n\nCONFIGURAZIONE & OPZIONI PACHETTI: I pacchetti preferiti nelle checklists verranno pre-selezionati. Utilizzare [Spazio] per selzionare e deselezionare."

_MntBody="Usare [Spazio] per de/selezionare le opzioni di montaggio desiderate e leggere accuratamente. Non selezionare multiple versioni della stessa opzione."
_MntConfBody="\nConfermare le seguenti opzioni di montaggio:"

# Mountpoint UEFI
_MntUefiBody="\nSelezionare mountpoint EFI.\n\nsystemd-boot richiede /boot. grub richiede /boot/efi."

# Partizionamento automatico
_PartBody1="Attenzione: TUTTI i dati contenuti in"
_PartBody2="saranno eliminati.\n\nVerrà creata una vfat/fat32 partizione di boot da 512MB, seguita da una seconda ext4 partizione (root o '/') usando tutto lo spazio rimanente"
_PartBody3="\n\nSi desidera continuare?"

# Messaggi d'errore. Tutti gli altri sono generati da BASH.
_ErrNoMount="\nLa/Le partizione/i deve/devono essere montata/e per prima/e.\n"
_ErrNoBase="\n$DIST base deve essere istallata per prima.\n"
_ErrNoConfig="\nLa configurazione di base per il sistema deve essere eseguita per prima.\n"

# Selezionare i file di configurazione
_EditTitle="Controllare i file di configurazione"
_EditBody="\nL'installazione è quasi finita.\n\nSelezionare ogni file elencato di seguito per essere controllato o modificato."

# Install BIOS Bootloader
_InstBootTitle="Installazione Bootloader"
_MntBootBody="\nGrub è consigliato per i principianti. E' possibile specificare il dispositivo sul quale installarlo.\n"
_InstSysTitle="Installa Syslinux"
_InstSysBody="\nInstallare syslinux nel Master Boot Record (MBR) on in Root (/)?\n"

# LUKS / DM-Crypt / Encryption
_LuksMenuBody="\nI dispositivi e volumi crittografati con dm_crypt non possono essere montati o visualizzati senza prima essere sbloccati da una chiave o password."
_LuksMenuBody2="\n\nÈ necessaria una partizione di boot separata senza cifratura o Logical Volume Management (LVM - tranne GRUB su BIOS)."
_LuksMenuBody3="\n\nL'opzione Automatica usa impostazioni crittografiche tipiche, ed è raccomandata per i principianti. Eventualmente, è possibile specificare algoritmo e grandezza chiave manualmente."
_LuksOpen="Apri Partizione Crittografata"
_LuksOpenBody="\nImmettere un nome e una password per il dispositivo crittografato.\n\nNon è necessario sia preceduto da /dev/mapper. Viene mostrato un esempio."
_LuksEncrypt="Crittografia LUKS Automatica"
_LuksEncryptAdv="impostare lunghezza chiave e algoritmo di cifratura"
_LuksEncryptBody="\nSelezionare una partizione da crittografare."
_LuksEncryptSucc="\nFatto! Aerta e pronta per LVM (raccomandato) o montaggio diretto.\n"
_LuksPartErrBody="\nDevono essere crittografate almeno due partizioni:\n\n1. Root (/) - ammesse partizioni standard o lvm.\n\n2. Boot (/boot or /boot/efi) - solo partizioni standard (o lvm con Grub su BIOS).\n"
_LuksOpenWaitBody="\nCreazione partizione Root crittografata in corso:"
_LuksCreateWaitBody="\nCreazione partizione Root crittografata in corso:"
_LuksWaitBody2="Volume o dispositivo in uso:"
_LuksCipherKey="\nUna volte modificati i parametri, saranno automaticamente utilizzati per il comando 'cryptsetup -q luksFormat /dev/...' .\n\nNOTA: l'uso di key files non è supportato; potranno essere aggiunti manualmente al termine dell'installazione. Non specificare parametri addizionali come -v (--verbose) o -y (--verify-passphrase).\n"

# Gestore logico dei volumi
_LvmMenu="\nLogical Volume Management (LVM) permette dischi fissi 'virtuali' (Volume Groups) e partizioni (Logical Volumes) creati da dispositivi e partizioni esistenti. Deve prima essere creato un Volume Group, pooi uno o più Logical Volumes al suo interno.\n\nLVM può essere utilizzato con una partizione crittografata per creare più volumi logici (ad es. root e home) al suo interno."
_LvmCreateVG="Crea VG e uno o più LV"
_LvmDelVG="Cancella Volume Groups"
_LvMDelAll="Cancella *TUTTI* i VG, LV, PV"
_LvmDetBody="\nTrovato Logical Volume Management (LVM) preesistente. Attivazione in corso. Attendere...\n"
_LvmPartErrBody="\nNon vi è alcuna partizione utilizzabile per il gestore logico dei volumi. Ne è necessaria almeno una.\n\n Se LVM è già in uso, la sua disattivazione permetterà di riutilizzarne le partizioni impiegate come Volumi Fisici.\n"
_LvmNameVgBody="\nInserisci un nome per il nuovo Gruppo di Volumi (VG).\n\nIl VG è il nuovo 'dispositivo virtuale / hard-disk' che verrà utilizzato, successivamente, per la creazione delle partizioni.\n"
_LvmNameVgErr="\nIl nome inserito non è valido. Il nome del Gruppo di Volumi può essere alfanumerico ma non può contenere spazi, iniziare con il carattere '/' o essere già stato assegnato.\n"
_LvmPvSelBody="\nSelezionare le partizioni da usare per il Volume Fisico (PV).\n"
_LvmPvConfBody1="\nConferma la creazione del Gruppo di Volumi "
_LvmPvConfBody2="con le seguenti partizioni:\n\n"
_LvmPvActBody1="\nCreazione ed attivazione del Gruppo di Volumi in corso "
_LvmPvDoneBody1="\nIl Gruppo di Volumi "
_LvmPvDoneBody2="è stato creato"
_LvmLvNumBody1="\nUsaro [Spazio] per selezionare il numero di Volumi Logici da creare(LVs) in"
_LvmLvNumBody2="\n\nL'ultimo (o l'unico) LV userà il 100% dello spazio disponibile nel Volume Group"
_LvmLvNameBody1="\nInserisci un nome per il nuovo Volume Logico (LV).\n\nQuesta operazione equivale ad assegnare un nome / una etichetta ad una partizione.\n"
_LvmLvNameBody2="\nATTENZIONE: Questo LV utilizzerà automaticamente tutto lo spazio rimanente nel Gruppo di Volumi"
_LvmLvNameErrBody="\nIl nome inserito non è valido. Il nome del Volume Logico (LV) può essere alfanumerico ma non può contenere spazi o iniziare con il carattere '/'.\n"
_LvmLvSizeBody1="rimanenti"
_LvmLvSizeBody2="\n\nInserisci la dimensione del Volume Logico (LV) in Megabyte (M) o Gigabyte (G). Ad esempio, 100M creerà un Volume Logico con dimensione pari a 100 Megabyte. 10G creerà un Volume Logico con dimensione pari a 10 Gigabyte.\n"
_LvmLvSizeErrBody="\nIl valore immesso non è valido. È necessario inserire un valore numerico che termini con una 'M' (Megabyte) o una 'G' (Gigabyte).\n\nAd esempio, 100M, 10G, o 250M. Il valore non deve inoltre essere maggiore o uguale dello spazio rimanente nel VG.\n"
_LvmCompBody="\nFatto! Sono stati creati tutti i Volumi Logici per il Gruppo di Volumi.\n\nVuoi visionare la nuova struttura del LVM?\n"
_LvmDelQ="\nConferma cancellazione dei Volume Group e dei Logical Volumes.\n\nIf cancellando un Volume Group, tutti i Logical Volumes al suo interno saranno cancellati.\n"
_LvmSelVGBody="\nSelezionare il Volume Group da cancellare. Tutti i  Logical Volumes al suo interno saranno cancellati.\n"
_LvmVGErr="\nNon sono stati trovati Volume Groups.\n"

# Controllo Requisiti
_NotRoot="\nL'installer deve essere eseguito come Root.\n"
_NoNetwork="\nIl test della connessione ad Internet è fallito.\n"

# Impostazione tastiera (vconsole)
_CMapTitle="Imposta Virtual Console"
_CMapBody="\nUna virtual console è un prompt di comando in un ambiente non-grafico. La sua mappatura tastiera è indipendente da quella per l'ambiente desktop / terminale."

# Impostazione Xkbmap (ambiente desktop)
_XMapBody="\nSeleziona la disposizione della testiera."

# Impostazione Localizzazione
_LocaleBody="I Locales determinano la lingua mostrata, i formati di data e ora, ecc.\n\nIl formato è lingua_NAZIONE (en_US per l'inglese, Sati Uniti; en_GB per inglese, Gran Bretagna)."

# Impostazione fuso orario
_TimeZBody="\nIl fuso orario è utilizzato per impostare correttamente l'ora del sistema."
_TimeSubZBody="\nSeleziona la città più vicina alla tua posizione."
_TimeZQ="\nImposta Time Zone"

# Impostazione Hostname
_HostNameBody="\nL'hostname è utilizzato per identificare il sistema all'interno di una rete.\n\nPuò essere composto di soli caratteri alfanumerici, può inoltre contenere un trattino (-) - ma non all'inizio o alla fine del nome."

# Impostare la password di Root
_RootBody="-- Inserisci la password di root (vuoto usa il precedente) --"

# Crea un nuovo utente
_UserTitle="Crea Nuovo Utente"
_UserBody="\nInserisci il nome e la password per il tuo nuovo account utente.\n\nIl nome non deve utilizzare lettere maiuscole, contenere alcun punto (.), Terminare con un trattino (-), o includere qualsiasi punto (:)\n\nNOTA: [Scheda] per alternare tra input e pulsanti, o premere [Invio] per accettare."
_UserErrTitle="Errore Nome Utente"
_UserErrBody="\nE' stato scelto un nome utente non valido. Provare nuovamente.\n"
_UserPassErr="\nLe password utente inserite non corrispondono.\n"
_RootPassErr="\nLe password di root inserite non corrispondono.\n"
_UserSetBody="\nCreazione utente ed impostazioni gruppi..\n"
_Username="Nome utente:"
_Password="Parola d'ordine:"
_Password2="Parola d'ordine2:"

# Montaggio (Partizioni)
_MntTitle="Stato Montaggio"
_MntSucc="\nMontaggio corretto!\n"
_MntFail="\nMontaggio fallito!\n"
_WarnMount="\nIMPORTANTE: Le aprtizioni possono essere montate senza formattarle selezionando '$_Skip' l'opzione mostrata in cima al menù file system.\n"

# Seleziona Dispositivo (installazione)
_DevSelTitle="Seleziona Dispositivo"
_DevSelBody="\nI Dispositivi (/dev/) sono i dishci dissi e le memorie USB disponibili per l'installazione. Il primo è /sda, il secondo /sdb, e così via."

# Tool Partizionamento
_PartTitle="Tool Partizionamento"
_PartBody="\nIl partizionamento automatico è disponibile per i principianti, altrimenti gparted è fornito come opzione GUI e cfdisk/parted per CLI.\n\nI sistemi UEFI richiedono una partizione vfat/fat32 tra 100-512M di dimensione da montare su /boot o /boot/efi, inoltre i sistemi BIOS che utilizzano LUKS richiedono una partizione separata /boot, compresa tra 100-512M e formattata come ext3/4."
_PartAuto="Partizionamento Automatico"
_PartWipe="Cancellazione Sicura Dispositivo (opzionale)"
_PartWipeBody1="\nATTENZIONE: TUTTI I DATI su"
_PartWipeBody2="saranno cancellati permanentemente tramite il comando 'wipe -Ifre'. Questo processo potrebbe richiedere molto tempo, a seconda della dimensione del dispositivo.\n\nContinuare?\n"

# Errore Partizionamento
_PartErrBody="\nI sistemi BIOS richiedono almeno una partizione (ROOT).\n\nI sistemi UEFI richiedono almeno due partizioni (ROOT e UEFI).\n"

# File System
_FSTitle="Selezione Filesystem"
_FSBody="\nExt4 è la scelta consigliata.\n\nNon tutti i filesystem sono utilizzabili per la partizione di root o quella di boot. Ciascuno dispone di funzionalità e limitazioni differenti."

# Select Root
_SelRootBody="\nSelezionare partizione ROOT. $DIST sarà installata qui."

# Select SWAP
_SelSwpBody="\nSelezionare partizione SWAP. Se si opta per uno Swapfile, sarà creato della stessa dimensione della RAM."
_SelSwpFile="File di Swap"
_SelSwpNone="Nessuna"

# Select UEFI
_SelUefiBody="\nSelezionare partizione UEFI. È una partizione speciale per l'avvio nei sistemi UEFI."
_FormUefiBody="[IMPORTANTE]\nLa partizione EFI"
_FormBiosBody="[IMPORTANTE]\nLa partizione boot"
_FormUefiBody2="è già formattato correttamente.\n\nVuoi saltare la formattazione? Scegliendo 'No' verranno cancellati TUTTI i dati (bootloader) sulla partizione.\n\nScegli incerto 'Sì'\n"


# Extra Partitions
_ExtPartBody="\nSeleziona le partizioni addizionali in qualsiasi ordine, altrimenti scegli 'Fatto' per concludere."
_ExtPartBody1="\nSpecificare mountpoint partizione. Assicurarsi che il nome cominci con uno slash (/). Ad esempio:"
_ExtErrBody="\nImpossibile montare la partizione a case di un problema con il nome mountpoint. Deve essere indicato un nome dopo lo slash.\n"

# Preparation Menu
_PrepTitle="Preparazione Installazione"
_PrepBody="\nOgni passaggio deve essere eseguito IN ORDINE. Una volta completati, selezionare 'Fatto' per finalizzare correttamente l'installazione.\n\nIl layout tastiera console verrà utilizzato sia per l'installer che per il sistema installato."
_PrepParts="Partizionamento Disco"
_PrepShowDev="Elenca i dispositivi (opzionale)"
_PrepLayout="Configura la disposizione della tastiera"
_PrepLUKS="Crittografia LUKS (opzionale)"
_PrepLVM="Logical Volume Management (opzionale)"
_PrepMount="Montaggio partizioni"
_PrepConfig="Configura installazione"
_PrepInstall="Installazione $DIST"

_BootLdr="Installa il Bootloader"

# Configure Base Menu
_ConfTitle="Configurazione di base"
_ConfBody="\nConfigurazione base del sistema di base."
_ConfHost="Imposta Hostname"
_ConfLocale="Imposta il linguaggio del sistema"
_ConfRoot="Imposta la password di Root"
_ConfUser="Aggiungi nuovo/i utente/i"

# Chiudere il programma di istallazione
_CloseInst="Chiudi il programma di installazione"
_CloseInstBody="\nSmontare le partizioni e chiudere l'installazione?"
_InstFinBody="\nL'installazione è terminata.\n\nVolete chiudere l'installazione e riavviare?"

# vim:tw=9999:syntax=off:nospell
