# Genérico
_All="Todos"
_Done="Finalizar"
_Skip="Saltar/Ninguno"
_ErrTitle="Error"
_NoFileErr="\nEl archivo no existe.\n"
_PlsWait="\nPor favor espere...\n"
_PassErr="\nLas contraseñas introducidas no coinciden.\n"
_Pass2="\nVuelva a escribir la contraseña para"
_TryAgain="Vuelva a intentarlo.\n"
_Exit="Saliendo.\n"
_Back="Atrás"
_Name="Nombre:"

# Bienvenido
_WelTitle="Bienvenido a"
_WelBody="\nEste instalador descargará los paquetes más recientes de los repositorios de $DIST. Sólo se realiza la configuración mínima necesaria.\n\nOPCIONES DE MENÚ: Seleccione pulsando el número de opción o usando las teclas flecha arriba/flecha abajo antes de pulsar [Intro] para confirmar. Cambie entre distintos botones con la tecla [Tabulador] o las teclas flecha izquierda/derecha antes de pulsar [Intro] para confirmar. Se puede navegar por listas largas utilizando las teclas [Re Pág] y [Av Pág], o bien pulsando la primera letra de la opción deseada.\n\nOPCIONES DE CONFIGURACIÓN Y PAQUETES: Los paquetes por defecto en las listas de verificación estarán premarcados. Utilice la tecla [BarraEspaciadora] para des/marcar."

_MntBody="Utilice la [BarraEspaciadora] para de/seleccionar las opciones de montaje deseadas y revisarlas cuidadosamente. No seleccione varias versiones de la misma opción."
_MntConfBody="\nConfirme las siguientes opciones de montaje:"

# Punto de montaje UEFI
_MntUefiBody="\nSeleccione el punto de montaje EFI.\n\nsystemd-boot requiere /boot. grub requiere /boot/efi."

# Autopartición
_PartBody1="Aviso: TODA la información en "
_PartBody2="será eliminada por completo.\n\nPrimero se creará una vfat/fat32 partición de arranque de 512MB, seguida por una segunda ext4 partición (raíz o '/') que utilizará todo el espacio restante"
_PartBody2="\n\n¿Desea continuar?"

# Mensajes de Error. Todos los demás son generados por BASH.
_ErrNoMount="\nPrimero se debe/n montar la/s partición/es.\n"
_ErrNoBase="\nPrimero se debe instalar el sistema base de $DIST.\n"
_ErrNoConfig="\nLa configuración básica del sistema debe hacerse primero.\n"

# Seleccionar archivos de configuración
_EditTitle="Revisar archivos de configuración"
_EditBody="\nLa instalación está casi terminada.\n\nSeleccione cualquier archivo de la lista para revisarlo o modificarlo."

# Instalar gestor de arranque
_InstBootTitle="Instalar gestor de arranque"
_MntBootBody="\nSe recomienda Grub para usuarios principiantes. También se puede seleccionar el dispositivo de instalación.\n"
_InstSysTitle="Instalar Syslinux"
_InstSysBody="\n¿Desea instalar 'syslinux' en el Registro de Arranque Maestro (MBR) o en la raíz (/)?\n"

# LUKS / DM-Crypt / Encriptación
_LuksMenuBody="\nLos dispositivos y volúmenes encriptados mediante 'dm_crypt' no son accesibles o incluso visibles sin antes desbloquearlos mediante una llave o contraseña."
_LuksMenuBody2="\n\nSe requiere una partición de arranque separada del resto, sin encriptación o gestión de volúmenes lógicos (LVM - a menos que se use grub BIOS)."
_LuksMenuBody3="\n\nLa opción 'Automático' utiliza opciones de encriptación predeterminadas, y es recomendable para principiantes. En cualquier caso, se puede ajustar manualmente el cifrado y el tamaño de la clave."
_LuksOpen="Abrir partición encriptada."
_LuksOpenBody="\nIngrese un nombre y una contraseña para el dispositivo encriptado.\n\nNo es necesario añadirle delante '/dev/mapper/'. Se proporciona un ejemplo para verlo mejor."
_LuksEncrypt="Encriptación LUKS automática"
_LuksEncryptAdv="Establecer cifrado y tamaño de la clave"
_LuksEncryptBody="\nSeleccione una partición para encriptar."
_LuksEncryptSucc="\n¡Listo! Abierto y listo para LVM (recomendado) o montaje directo.\n"
_LuksPartErrBody="\nSe requiere un mínimo de dos particiones para la encriptación:\n\n1. Raíz (/) - tipos de partición estándar o LVM.\n\n2. Arranque (/boot o /boot/efi) - sólo tipos de partición estándar (excepto LVM cuando se utiliza Grub BIOS).\n"
_LuksOpenWaitBody="\nCreando partición raíz encriptada:"
_LuksCreateWaitBody="\nCreando partición raíz encriptada:"
_LuksWaitBody2="Dispositivo o volumen en uso:"
_LuksCipherKey="\nUna vez que se hayan ajustado los flags especificados, se utilizarán de forma automática con el comando 'cryptsetup -q luksFormat /dev/...'\n\nNOTA: los archivos de claves no están soportados; se pueden añadir manualmente después de la instalación. No especifique flags adicionales tales como -v (--verbose) o -y (--verify-passphrase).\n"

# Gestión de volúmenes lógicos
_LvmMenu="\nLa gestión de volúmenes lógicos (LVM) permite crear discos duros (grupos de volúmenes) y particiones (volúmenes lógicos) 'virtuales' a partir de dispositivos y particiones existentes. Primero se debe crear un grupo de volúmenes, y después uno o más volúmenes lógicos dentro de éste.\n\nTambién se puede usar LVM con una partición encriptada para crear varios volúmenes lógicos (e.g raíz y home) dentro de ésta."
_LvmCreateVG="Crear grupos de volúmenes y volúmenes lógicos"
_LvmDelVG="Borrar grupos de volúmenes"
_LvMDelAll="Borrar *TODOS* los GVs, VLs y VPs"
_LvmDetBody="\nSe ha detectado un LVM ya existente. Activando. Por favor, espere...\n"
_LvmPartErrBody="\nNo hay particiones disponibles para ser usadas para la gestión de volumen lógico. Se necesita una como mínimo.\n\nSi LVM ya está en uso, desactivarlo permitirá a la o las particiones usadas para los volúmenes físicos ser usadas otra vez.\n"
_LvmNameVgBody="\nIntroduzca el nombre del grupo de volúmenes (GV) a crear.\n\nEl grupo de volúmenes (GV) es el nuevo 'dispositivo virtual' o 'disco duro'.\n"
_LvmNameVgErr="\nNombre introducido no válido. El nombre del grupo de volúmenes puede ser alfanumérico, pero no puede contener espacios, empezar con '/' o estar ya en uso.\n"
_LvmPvSelBody="\nSeleccione la o las particiones a usar por el volúmen físico (VF).\n"
_LvmPvConfBody1="\nConfirmar la creación del grupo de volúmenes "
_LvmPvConfBody2="con las siguientes particiones:\n\n"
_LvmPvActBody1="\nCreando y activando grupo de volúmenes "
_LvmPvDoneBody1="\nEl grupo de volúmenes "
_LvmPvDoneBody2="se ha creado"
_LvmLvNumBody1="\nUtilice [BarraEspaciadora] para elegir el número de volúmenes lógicos (VLs) que crear"
_LvmLvNumBody2="\n\nEl último (o único) VL usará de forma automática el 100% del espacio restante en el grupo de volúmenes."
_LvmLvNameBody1="\nIntroduzca el nombre del volumen lógico (VL) a crear.\n\nEs como asignar un nombre o una etiqueta a una partición.\n"
_LvmLvNameBody2="\nATENCIÓN: Este volumen lógico utilizará automáticamente todo el espacio restante del grupo de volúmenes"
_LvmLvNameErrBody="\nEl nombre introducido no es válido. El nombre del volumen lógico (VL) debe ser alfanumérico, pero no puede contener espacios o empezar con '/'.\n"
_LvmLvSizeBody1="restante"
_LvmLvSizeBody2="\n\nIntroduzca el tamaño del volumen lógico (VL) en megabytes (M) o gigabytes (G). Por ejemplo, '100M' creará un volumen lógico de 100MB. '10G' creará un volumen lógico de 10GB.\n"
_LvmLvSizeErrBody="\nEl valor introducido no es válido. Un valor numérico debe ser introducido con una 'M' (Megabytes) o una 'G' (Gigabytes) al final.\n\nPor ejemplo: '100M', '10G' o '250M'. El valor tampoco puede ser mayor o igual que el tamaño restante del grupo de volúmenes.\n"
_LvmCompBody="\n¡Listo! Todos los volúmenes lógicos han sido creados para el grupo de volúmenes.\n\n¿Desea ver el nuevo esquema de LVM?\n"
_LvmDelQ="\nConirmar eliminación de grupo/s de volúmenes y volúmen/es lógico/s.\n\nSi se borra un grupo de volúmenes, todos los vol. lógicos que contenga se eliminarán tambien.\n"
_LvmSelVGBody="\nSelecciona el grupo de volúmenes a eliminar. Todos los volúmenes lógicos serán eliminados también.\n"
_LvmVGErr="\nNo se han encontrado grupos de volúmenes.\n"

# Comprobar requisitos
_NotRoot="\nEl instalador debe ser ejecutado como superusuario (usuario root).\n"
_NoNetwork="\nFallo de la prueba de conexión a internet.\n"

# Seleccionar distribución de teclado de la consola virtual (vconsole)
_CMapTitle="Seleccionar distribución de teclado de la consola virtual"
_CMapBody="\nUna consola virtual es un intérprete de comandos en un entorno no gráfico. Su distribución de teclado es independiente de un entorno de escritorio o terminal."

# Seleccionar Xkbmap (entorno)
_XMapBody="\nSeleccionar distribución de teclado del entorno de escritorio."

# Seleccionar localización
_LocaleBody="La localización (locale) determina los idiomas para mostrar, los formatos de fecha y hora, etc.\n\nEl formato es idioma_PAÍS (e.g es_ES significa español, España; es_MX significa español, México)."

# Establecer zona horaria
_TimeZBody="\nLa zona horaria es usada para ajustar el reloj del sistema correctamente."
_TimeSubZBody="\nSeleccione la ciudad más cercana a usted."
_TimeZQ="\nEstablecer zona horaria como"

# Establecer nombre del equipo
_HostNameBody="\nEl nombre del equipo se usa para identificar al sistema dentro de una red.\n\nEstá restringido a carcteres alfanuméricos, y puede contener guiones (-) pero no al principio o al final del nombre"

# Establecer contraseña de superusuario
_RootBody="--- Ingrese la contraseña de root (vacía usa la de arriba) ---"

# Crear nuevo usuario
_UserTitle="Crear un nuevo usuario"
_UserBody="\nIngrese el nombre y la contraseña de su nueva cuenta de usuario.\n\nEl nombre no debe usar letras mayúsculas, contener ningún punto (.), Terminar con un guión (-) o incluir dos puntos (:)\n\nNOTA: [Pestaña] para alternar entre la entrada de texto y los botones, o presione [Intro] para aceptar."
_UserErrTitle="Error de nombre de usuario"
_UserErrBody="\nSe ha introducido un nombre de usuario incorrecto. Vuelva a intentarlo.\n"
_UserPassErr="\nLas contraseñas de usuario ingresadas no coinciden.\n"
_RootPassErr="\nLas contraseñas de raíz ingresadas no coinciden.\n"
_UserSetBody="\nCreando usuario y ajustando grupos...\n"
_Username="Nombre de usuario:"
_Password="Contraseña:"
_Password2="Contraseña2:"

# Montaje (particiones)
_MntTitle="Estado de montaje"
_MntSucc="\n¡Montaje realizado con éxito!\n"
_MntFail="\n¡Montaje fallido!\n"
_WarnMount="\nIMPORTANTE: las particiones se pueden montar sin formatearlas seleccionando la opción '$_Skip' listada al principio del menú de sistemas de archivos.\n"

# Seleccionar dispositivo (instalación)
_DevSelTitle="Seleccionar dispositivo"
_DevSelBody="\nLos dispositivos (/dev) son los discos duros y unidades USB disponibles para la instalación. El primero es /sda, el segundo es /sdb, y así sucesivamente."

# Herramienta de particionado
_PartTitle="Herramienta de particionado"
_PartBody="\nEl partición automática está disponible para principiantes, de lo contrario, gparted se proporciona como una opción de GUI y se cfdisk/parted para CLI.\n\nLos sistemas UEFI requieren una partición vfat/fat32 entre 100-512M de tamaño para ser montada en /boot o /boot/efi, adicionalmente los sistemas BIOS que usan LUKS requieren una partición separada /boot, entre 100-512M y formateada como ext3/4."
_PartAuto="Particionado automático"
_PartWipe="Borrar dispositivo de forma segura (opcional)"
_PartWipeBody1="\nAVISO: TODOS los datos en"
_PartWipeBody2="serán eliminados por completo mediante el comando 'wipe -Ifre'. Este proceso puede llevar mucho tiempo dependiendo del tamaño del dispositivo.\n\n¿Desea continuar?\n"

# Error de particionado
_PartErrBody="\nLos sistemas BIOS requieren un mínimo de una partición (RAÍZ).\n\nLos sistemas UEFI requieren un mínimo de dos particiones (RAÍZ y UEFI).\n"

# Sistema de archivos
_FSTitle="Elegir sistema de archivos"
_FSBody="\nSe recomienda el sistema de archivos Ext4.\n\nNo todos los sistemas de archivos son adecuados para particiones raíz o de arranque. Todas tienen sus características y limitaciones."

# Seleccionar raíz
_SelRootBody="\nSeleccione la partición RAÍZ. Aquí es donde se instalará $DIST."

# Seleccionar SWAP
_SelSwpBody="\nSeleccione la partición SWAP. Si va a usar un archivo SWAP, se creará con un tamaño igual al de la memoria RAM del equipo."
_SelSwpFile="Archivo SWAP"
_SelSwpNone="Ninguno"

# Seleccionar UEFI
_SelUefiBody="\nSeleccione la partición UEFI. Ésta es una partición especial para arrancar los sistemas UEFI."
_FormUefiBody="[IMPORTANTE]\nLa partición EFI"
_FormBiosBody="[IMPORTANTE]\nLa partición boot"
_FormUefiBody2="ya está formateado correctamente.\n\n¿Quieres omitir el formateo? Elegir 'No' borrará TODOS los datos (cargadores de arranque) en la partición.\n\nSi no está seguro, elija 'Sí'.\n"


# Particiones extra
_ExtPartBody="\nSeleccione particiones adicionales en cualquier orden, o 'Finalizar' para terminar el proceso."
_ExtPartBody1="\nPunto de montaje de partición específica. Asegúrese de que el nombre empieza con una barra inclinada (/). Ejemplos:"
_ExtErrBody="\nLa partición no se puede montar debido a un problema con el nombre del punto de montaje. Se debe indicar un nombre después de una barra inclinada (/).\n"

# Menú de preparación
_PrepTitle="Preparar instalación"
_PrepBody="\nCada paso se ha de seguir en ESTRICTO ORDEN. Una vez que haya terminado, seleccione 'Finalizar' para terminar la instalación correctamente.\n\nLa distribución de teclado de la consola se utilizará tanto para el instalador como para el sistema instalado."
_PrepLayout="Establecer distribución de teclado del escritorio."
_PrepParts="Particionar disco"
_PrepShowDev="Listar dispositivos (opcional)"
_PrepLUKS="Encriptación LUKS (opcional)"
_PrepLVM="Gestión de volúmenes lógicos (LVM) (opcional)"
_PrepMount="Montar particiones"
_PrepConfig="Configurar instalación"
_PrepInstall="Instalar $DIST"

_BootLdr="Instalar gestor de arranque"

# Menú de configuración del sistema base
_ConfTitle="Configurar sistema base"
_ConfBody="\nConfiguración básica del sistema base."
_ConfHost="Establecer nombre del equipo"
_ConfTimeHC="Establecer zona horaria y reloj"
_ConfLocale="Establecer idioma del sistema"
_ConfRoot="Establecer contraseña de superusuario"
_ConfUser="Añadir nuevo/s usuario/s"

# Cerrar instalador
_CloseInst="Cerrar instalador"
_CloseInstBody="\nDesmontar particiones y cerrar el instalador?"
_InstFinBody="\nLa instalación ahora está terminada.\n\n¿Le gustaría cerrar el instalador y reiniciar?"

# vim:tw=9999:syntax=off:nospell
